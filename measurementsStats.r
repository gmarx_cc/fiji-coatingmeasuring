library(ggplot2)
options(max.print=20)
myData<-read.csv("plotExample.csv", header=TRUE)
summary(myData)
ggplot(data=myData, aes(myData$Y0)) + 
  geom_histogram(aes(y =..density..), 
                 breaks=seq(0, 150, by = 2), 
                 col="red", 
                 fill="green", 
                 alpha = .2) + 
  geom_density(col=2) + 
  labs(title="Histogram for Age") +
  labs(x="Age", y="Count")

#myData<-myData[-c(1)]
#cor(myData, use="complete.obs", method = c("pearson"))
#cor(myData, use="complete.obs", method = c("spearman"))
###var(myData, use="complete.obs")


